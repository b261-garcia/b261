package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity2 {

    public static void main(String[] args){

        int[] primeNumbers = {2,3,5,7,11};
        System.out.println("The first prime number is: " + primeNumbers[0]);

        ArrayList<String> names = new ArrayList<>(); //ArrayList<String> names = new ArrayList<>(Arrays.asList("John","Jane","Chloe","Zoey"));

        names.add("John");
        names.add("Jane");
        names.add("Chloe");
        names.add("Zoey");

        System.out.println("My friends are: " + names);

        HashMap<String, Integer> items = new HashMap<>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }

        };

        System.out.println("Our current inventory consists of: " + items);








    }
}
