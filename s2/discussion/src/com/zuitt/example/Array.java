package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {

    //Java Collection
        //is a single of unit of objects
        //useful for manipulating relevant pieces of data that can be used in different situations
    public static void main(String[] args){

        //Array
            //Are containers of values of the sama "data type" given a pre-defined amount of values
            //Java arrays are more rigid, once the sie and data type are defined, they can no longer be change
        //Syntax: Array Declaration
            //datatype[] identifier = new dataType[numberOfElements]
            //the values of the array is initializes to 0 or null
        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 98;

        System.out.println(intArray[2]);

        ///It prints the memory address of the array
        System.out.println(intArray);

        //To print the intArray "all element" (Arrays.toString)
        System.out.println(Arrays.toString(intArray));

        //Declaring Array with Initialization
        //Syntax:
            //datatype[] indetifier = {elementA, elementB, .....elementC};
        String[] names = {"John", "Jane", "Joe"};
        System.out.println(Arrays.toString(names));

        //Multi-dimensional Arrays
        //Syntax
            //datatype[][] identifier = new datatype[rowLength][columnLength];

        String[][] classroom = new String[3][3];

        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Jane";
        classroom[1][2] = "Jobert";
        //Third Rown
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofey";

        System.out.println(Arrays.toString(classroom));
        //To print the classroom (Arrays.deepToString)
        System.out.println(Arrays.deepToString(classroom));

        //ArrayLists
            //Are resizable arrays, wherein elements can be added or removed whenever it is needed.
        //Syntax:
           //ArrayList<dataType> identifier = new ArrayList<dataType>();

        //Decalre an arrayList
        ArrayList<String> students = new ArrayList<>();

        //To add elements in ArrayList using ".add()" function
        students.add("John");
        students.add("Paul");

        System.out.println(students);

        //Declare an ArrayList with values
        ArrayList<String> students1 = new ArrayList<>(Arrays.asList("John", "Paul"));

        System.out.println(students1);
        //To get elements in ArrayList using ".get()" function
        System.out.println(students1.get(1));

        //Updating an element
        students1.set(1, "George");
        System.out.println(students1);

        //Adding an element on a specific index
        students1.add(1, "Mike");
        System.out.println(students1);

        //Removing an element on a specific index
        students1.remove(1);
        System.out.println(students1);

        //Removing all elements
        students1.clear();
        System.out.println(students1);

        //Getting the number of elements in ArrayList
        System.out.println(students1.size());

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1,2,5));
        System.out.println(numbers);

        //HashMaps
            //A collection of data in "Key-value paris"
            //in Java, ""keys" also referred by the "fields"
            //wherein the values are accessed by the "fields"
        //Syntax:
            //HashMap<dataTypeField, dataTypeValue> identifier = new HashMaps<dataTypeField, dataTypeValie>();

        //Declaring HashMaps
//        HashMap<String, String> jobPosition = new HashMap<>();
//
//        //Add elements using put() function in HashMap
//        jobPosition.put("Studend", "Alice");
//        jobPosition.put("Developer", "Magic");
//
//        System.out.println(jobPosition);

        //Declaring HashMaps with Initialization
        HashMap<String, String> jobPosition = new HashMap<>(){
            {
                put("Student", "Alice");
                put("Developrt", "Magic");
            }
        };

        System.out.println(jobPosition);

        //Accessing elements in Hashmaps
        System.out.println(jobPosition.get("Student"));

        //Updating an elements
        jobPosition.replace("Student", "Jacob");
        System.out.println(jobPosition);

        //Adding element but if key already existed it will updated the value instead of adding new element
        jobPosition.put("Student", "Jacob P");
        jobPosition.put("Teacher", "Janette");
        System.out.println(jobPosition);

        //Will show only the field names "key"
        System.out.println(jobPosition.keySet());

        //Will show only the values
        System.out.println(jobPosition.values());

        //Delete or Remove specifc key
        jobPosition.remove("Student");
        System.out.println(jobPosition);

        //Remove All
        jobPosition.clear();
        System.out.println(jobPosition);

    }

}
