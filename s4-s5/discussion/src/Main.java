//import com.zuitt.example.Car;
import com.zuitt.example.*;     //* to import all within the packages
public class Main {
    public static void main(String[] args) {

        //System.out.println("Hello world!");

        //OOP
            //stands for "Object-Oriented Programming"
            //OOP is a programming model that allows developers to design software around data or objects, rather than function and logic

        //OOP Concepts
            //Objects - abstract idea that represent something in the real world
            //Class - representation of the object using code
            //Instance - unique copy of the idea, made "physical"

        //Objects
            //States and Attributes - what is the idea about
            //Behaviors - what can idea do?
            //Example: A Person has attributes like name, age , height, weight. "Behaviors"And a person can eat, sleep and speak.

        //Four Pillars of OOP
        // 1.Encapsulation
        // 2.Inheritance
        // 3.Abstraction,
        // 4.Polymorphism

        //1. Encapsulation
            //data hiding - the variables of a class will be hidden from the other classes and can be accessed only through the methods of the current class
        //provide a public setter and getter function

        //Create a car
        Car myCar = new Car();
        myCar.drive();

        //Assign properties of myCar using the setter methods
        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2030);

        //To view the properties of myCar using the getter methods
        System.out.println("Car name: " + myCar.getName());
        System.out.println("Car brand: " + myCar.getBrand());
        System.out.println("Car year of make: " + myCar.getYearOfMake());

        //Composition and Inheritance
            //Both concepts promote code re-use through different approach
            //"Inheritance" allows modelling an object that is a subset of another object
                //It defines "is a relationship"
                //To design a class on what it is

            //"Composition" allows modelling an object that are made up of other objects
                //both entities are dependent on each other
                //composed object cannot exist without the other entity
                //It defines "has a relationship"
                //To design a class on what it does
                //Example:
                    //A car is a vehicle - inheritance
                    //A car has a driver - composition

        System.out.println(("Driver name: " + myCar.getDriverName()));
        myCar.setDriver("JOhn Kill");
        System.out.println(("Driver name: " + myCar.getDriverName()));

        //2. Inheritance
            //Can be defined as the process where one class acquires the properties and methods of another class
            //With the use of inheritance, the information is made manageable in our hierarchical order

        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();
        myPet.call();

        System.out.println("Pet name: " + myPet.getName() + ". Breed: " + myPet.getColor());

        //3. Abstraction
            //is a process where all the logic and complexity are hidden from the user.
            //The user would know what to do rather than how it is done

            //Interfaces
                //This is used to achieve total abstraction
                //Creating Abstract classes doesn't support "multiple inheritance", but it can be achieve with interface
                //Act as "contracts" wherein a class implements the interface and should have the methods that the interface has defined in the class.

        Person child = new Person();
        child.sleep();
        child.run();
        child.morningGreet();
        child.holidayGreet();


        //4. Polymorphism
            //Derived from the greek word: poly means "many" and morph means "forms".
            //In short "many forms"
            //is the ability of an object to take on many forms.
            //This is usually done by function/method overloading/overriding

        //Two Main types of Polymorphism
            //1. Static or Compile time polymorphism
            //method with the same name, but they have different data types and a different number of arguments

        StaticPoly myAddition = new StaticPoly();
            //original method
        System.out.println(myAddition.addition(5,6));
            //based on adding arguments
        System.out.println(myAddition.addition(5,6,8));
            //based on changing data types
        System.out.println(myAddition.addition(5.5,6.6));

            //2. Dynamic or Run-time Polymorphism
            //Function is overriden by replacing the definition of the method in the parent class to the child class.
            /*
                Parent Class                                Child
                    name                                    name
                    address                                 address

                    work("I am a developer")                work("I am manager")
             */

        Child myChild = new Child();
        myChild.speak();

    }
}