package com.zuitt.example;

//Chill class of Animal
    //"extends" keyword is used to inherit the properties and methods of the parent class
public class Dog extends Animal{
    //Property
    private String breed;
    //Constructor
    public Dog(){
        //super() is to have a direct access with the original constructor(Parent Class "Animal")
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name,color);
        this.breed = breed;
    }

    //Getter and Setter
    public String getBreed() {
        return this.breed;
    }
    public void setBreed(String breed){
        this.breed = breed;
    }

    //Method
    public void speak(){

        System.out.println("Woof, woof");
    }
    public void call(){
        super.call();
        System.out.println("Hi! My name is: " + this.getName() + ", I am dog.");
    }



}
