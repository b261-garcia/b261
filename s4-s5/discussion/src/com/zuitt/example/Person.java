package com.zuitt.example;

//Interface Implementation
    //THis is where we implement our Action interface
public class Person implements Actions, Greetings{

   public void sleep(){
       System.out.println("ZZZZZzzzzzzzzz..........");
   }


   public void run(){
        System.out.println("Running...");
   }

   public void morningGreet(){
        System.out.println("Good morning!");
   }

   public void holidayGreet(){
       System.out.println("Happy Holidays!");
   }

}
