//A "package" in Java is used to group related classes. Think of it as a folder in ourdirectory
    //Packages are divided into two categories
        //1. Built-in Packages - (packages from our JAVA API
        //2. User-defined Packages

//Package creation it follows the "reversed domain name notation for the naming convention"
package com.zuitt.example;

public class Variables {
    public static void  main(String[] args) {

            //Variables
            int age;
            char middleInitial;

            //Variable Declaration vs. Initialization
        int x;
        int y = 0;

        x = 1;
        //Output to the system "console.log"
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        //Primitive Data Types
        //predefined within the Java programming which is used for a "single-valued variables with limited capabilities

        //int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long used for longer numbers
        //L is being added at the end of the long number to be recognized
        long worldPopulation = 56416874383843898L;
        System.out.println(worldPopulation);

        //float
        //f is added at the end of the float  to be recognized
        float piFLoat = 3.21561655235325325f;
        System.out.println(piFLoat);

        //double
        double piDouble = 3.2156165236532623;
        System.out.println(piDouble);

        //char - single character
        //uses single quote
        char letter = 'a';
        System.out.println(letter);

        //Boolean true or false
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        isTaken = true;
        System.out.println(isTaken);

        //Constant = const = 'final' is used for const
        //Good practice to use 'uppercase' when using constant' 'final' identifier
        final int PRINCIPAL = 3000;

        //Non-primitive data type
            //also known as reference data types refer to instances or objects

        //String
            //Stores a sequences or array of characters
            //are actually object that can use methods

        String userName = "JSmith";
        System.out.println(userName);

        int stringLength = userName.length();
        System.out.println(stringLength);

    }
}
