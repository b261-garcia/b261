package com.zuitt.example;

import java.util.Scanner;
public class UserInput {

    public static void main(String[] args) {

        //Scanner is used for obtaining input in terminal.
        //System.in allows as to take the input from the console
        Scanner myObject = new Scanner(System.in);
        System.out.println("Enter Username:");

        //To capture the input given by the user, we use the nexline() method
        String userName = myObject.nextLine();
        System.out.println("Username is: " + userName);




    }
}
