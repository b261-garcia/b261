package com.zuitt.example;

import java.util.Scanner;
public class TypeConversion {

    public static void main(String[] args){

        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter numbers to add.");
        System.out.println("Enter a first numner: ");

        //int num1 = Integer.parseInt(myObj.nextLine());
        //shorter way
        int num1 = myObj.nextInt();


        System.out.println("Enter a second number");

        int num2 = myObj.nextInt();

        System.out.println("The sum of two number is " + (num1 + num2));
    }
}
