import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        ArrayList<Contact> contacts = new ArrayList<Contact>();

        Phonebook phonebook = new Phonebook(contacts);

        //Instantiate 2 contacts
        Contact john = new Contact("John Doe", "09152468596", "Quezon City");

        Contact jane = new Contact("Jane Doe", "09162148573", "Caloocan City");

        //Adding to phonebook object
        contacts.add(john);
        contacts.add(jane);

        if(phonebook.getContacts().isEmpty()){
            System.out.println("The phonebook is empty");
        } else {
            for (Contact contact: phonebook.getContacts()){
                System.out.println("--------------");
                System.out.println(contact.getName());
                System.out.println("--------------");
                System.out.println(contact.getName() +" has the following registered number: " + "\n" + contact.getContactNumber());
                System.out.println(contact.getName() +" has the following registered address: " + "\nmy home in " + contact.getAddress());
            }
        }
    }
}
