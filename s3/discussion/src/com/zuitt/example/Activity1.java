package com.zuitt.example;

import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        System.out.println("Enter an integer whose factorial will be computed: ");

        Scanner in = new Scanner(System.in);

        try{
            int num = in.nextInt();

            int answer = 1;
            int counter = 1;

            if(num >= 0){
                if(num == 0){
                    System.out.println("Factorial of 0 is 1");
                }else{
                    for(counter = 1; counter <= num; counter++){
                        answer *= counter;
                    }
                    System.out.println("The factorial of " + num + " is " + answer);
                }
            }else{
                System.out.println("Cannot compute for factorials of negative numbers...");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
