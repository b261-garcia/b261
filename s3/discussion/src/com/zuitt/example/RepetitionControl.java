package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
public class RepetitionControl {

    public static void main(String[] args) {
        //Loops
        //Are control structures that allow code blocks to be executed multiple times

        //While Loop
        int x=0;
        while(x<10){
            System.out.println(("Loop Number: " + x));
            x++;
        }

        //Do-While Loop
        //Similar to while Loops however, will execute atleast once.

        int y=10;
        do{
            System.out.println("Countdown: " + y);
            y--;
       }while(y>10);

        //For Loop
        //Syntax: for(initialValue; condition; iteration){
            //code block
        //}

        for(int i = 0; i < 10; i++){
            System.out.println("Current count: " + i);
        }

        //For Loop with Arrays
        int[] intArray = {100,200,300,400,500};

        for(int i = 0; i < intArray.length; i++){
            System.out.println(intArray[i]);
        }

        //For-each loop with Array
        //Syntax:
             /* for(dataType itemName: arrayName){
                    //code block
            }*/

        String[] nameArray = {"John","Paul", "George", "Ringo"};
        for(String name : nameArray){
            System.out.println(name);
        }

        //Nested for Loops
        String [][] classroom = new String[3][3];
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        classroom[1][0] = "Barandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goody";

        //Outer loop will loop through the rows
        for(int row = 0; row < 3; row++){
                //inner loop will loop through the columns of each row
            for(int col=0; col<3; col++){
                System.out.println(classroom[row][col]);
            }
        }

        for(String[] row:classroom){
            for(String column:row){
                System.out.println(column);

            }
        }

        //for-each with ArrayList
        /*Syntax:
            arrayListName.forEach(consumer -> //codeblock)
        */

        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        System.out.println("ArrayList: " + numbers);

        numbers.forEach(number -> System.out.println("ArrayList: " + number));

        //forEach with HashMaps
       /* Syntax:
            hashMapName.forEach((key,value) -> codeblock)
        */

        HashMap<String, Integer> grades = new HashMap<>(){
            {
                put("English", 90);
                put("Math", 95);
                put("Science", 97);
                put("Science", 97);
                put("History", 94);
            }
        };

        grades.forEach((subject,grade) -> System.out.println(subject + ": " + grade));




    }
}
